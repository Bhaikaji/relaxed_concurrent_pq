#include "shared_pq.h"

#include <gtest/gtest.h>
#include <thread>

const int NUM_THREADS = 16;
const int DATA_SIZE = 100000;

const int RELAXATION = 256;

static std::atomic<int> sum(0);
static std::atomic<int> ops(0);
static std::atomic<bool> stop_delete_min(false);
static std::atomic<bool> insert_barrier(false);
static std::atomic<bool> delete_barrier(false);
static std::vector<std::atomic<int>> result(DATA_SIZE *NUM_THREADS);

void reset_variables() {
  ops.store(0, std::memory_order_relaxed);
  sum.store(0, std::memory_order_relaxed);
  stop_delete_min.store(false, std::memory_order_relaxed);
  insert_barrier.store(false, std::memory_order_relaxed);
  delete_barrier.store(false, std::memory_order_relaxed);
  std::fill(result.begin(), result.end(), 0);
}

int relaxed_upper_bound(const int iteration, const int relaxation) {
  return relaxation + iteration;
}

void break_barrier(std::atomic_bool &barrier) {
  barrier.store(true, std::memory_order_relaxed);
}

void build_barrier(std::atomic_bool &barrier) {
  while (!barrier.load(std::memory_order_relaxed)) {
    // Wait.
  }
}

template <class K, class V, int Rlx>
void insert_helper(int thread_id, SharedPQ<K, V, Rlx> &spq, int start,
                   int count) {
  build_barrier(insert_barrier);
  for (int i = start; i < start + count; i++) {
    spq.insert(i, i);
  }
}

template <class K, class V, int Rlx>
void delete_helper(int thread_id, SharedPQ<K, V, Rlx> &spq) {
  V value;

  build_barrier(delete_barrier);

  while (!stop_delete_min) {
    if (spq.delete_min(value)) {
      ops.fetch_add(1, std::memory_order_relaxed);
      // This assertions is not possible in case of multiple threads.
      if (NUM_THREADS == 1) {
        ASSERT_LE(value, relaxed_upper_bound(ops, RELAXATION));
      }
      result[value].fetch_add(1, std::memory_order_relaxed);
      sum.fetch_add(value, std::memory_order_relaxed);
    }
  }
}

// void timer(std::chrono::seconds sec) {
//  LOG("Starting timer..");
//  break_barrier(start_barrier);
//  std::this_thread::sleep_for(sec);
//  done.store(true, std::memory_order_relaxed);
//}

TEST(SharedPQTest, SingleThreadInsertMultipleThreadDelete) {
  SharedPQ<int, int, RELAXATION> spq;
  std::thread threads[NUM_THREADS];

  reset_variables();

  // Add elements using the main thread.
  for (int i = 0; i < NUM_THREADS * DATA_SIZE; ++i) {
    spq.insert(i);
  }

  for (int i = 0; i < NUM_THREADS; ++i) {
    threads[i] =
        std::thread(delete_helper<int, int, RELAXATION>, i, std::ref(spq));
  }

  break_barrier(delete_barrier);

  while (ops < DATA_SIZE * NUM_THREADS) {
    // Wait until the queue is not empty.
  }

  stop_delete_min.store(true, std::memory_order_relaxed);

  for (auto &th : threads) {
    th.join();
  }

  ASSERT_EQ(ops, DATA_SIZE * NUM_THREADS);
  // Sum of series = N * N - 1 / 2
  int N = NUM_THREADS * DATA_SIZE;
  // This verifies the checksum of data deleted.
  ASSERT_EQ(sum, (N / 2) * (N - 1));
  // This verifies that each element is deleted only once.
  for (int i = 0; i < N; i++) {
    ASSERT_EQ(result[i], 1);
  }
}

TEST(SharedPQTest, MultipleThreadInsertMultipleThreadDelete) {
  SharedPQ<int, int, RELAXATION> spq;
  std::thread threads[NUM_THREADS];

  reset_variables();

  // Add elements using the multiple threads.
  for (int i = 0; i < NUM_THREADS; ++i) {
    threads[i] = std::thread(insert_helper<int, int, RELAXATION>, i,
                             std::ref(spq), i * DATA_SIZE, DATA_SIZE);
  }

  break_barrier(insert_barrier);

  // Wait for inserting threads to finish first.
  for (auto &th : threads) {
    th.join();
  }

  // Now verify delete_min as in case of single thread case.
  for (int i = 0; i < NUM_THREADS; ++i) {
    threads[i] =
        std::thread(delete_helper<int, int, RELAXATION>, i, std::ref(spq));
  }

  break_barrier(delete_barrier);

  while (!spq.empty()) {
    // Wait until the queue is not empty.
  }

  stop_delete_min.store(true, std::memory_order_relaxed);

  for (auto &th : threads) {
    th.join();
  }

  ASSERT_EQ(ops, DATA_SIZE * NUM_THREADS);
  // Sum of series = N * N - 1 / 2
  int N = NUM_THREADS * DATA_SIZE;
  // This verifies the checksum of data deleted.
  ASSERT_EQ(sum, (N / 2) * (N - 1));
  // This verifies that each element is deleted only once.
  for (int i = 0; i < N; i++) {
    ASSERT_EQ(result[i], 1);
  }
}

TEST(SharedPQTest, AlternatingInsertDeleteTestWithMultipleThreads) {
  // In case of a single thread, this test doesn't make sense.
  if (NUM_THREADS <= 1) {
    return;
  }
  SharedPQ<int, int, RELAXATION> spq;
  std::thread threads[NUM_THREADS];

  reset_variables();

  // Add elements using the multiple threads.
  for (int i = 0; i < NUM_THREADS; ++i) {
    // Every other thread is an inserting thread
    if (i % 2) {
      threads[i] =
          std::thread(insert_helper<int, int, RELAXATION>, i, std::ref(spq),
                      (i - 1) * DATA_SIZE, DATA_SIZE * 2);
    } else {
      threads[i] =
          std::thread(delete_helper<int, int, RELAXATION>, i, std::ref(spq));
    }
  }

  break_barrier(insert_barrier);
  break_barrier(delete_barrier);

  while (ops < NUM_THREADS * DATA_SIZE) {
    // Wait until all the elements are deleted.
  }

  stop_delete_min.store(true, std::memory_order_relaxed);

  // Wait for all threads to finish.
  for (auto &th : threads) {
    th.join();
  }

  ASSERT_EQ(ops, DATA_SIZE * NUM_THREADS);
  // Sum of series = N * N - 1 / 2
  int N = NUM_THREADS * DATA_SIZE;
  // This verifies the checksum of data deleted.
  ASSERT_EQ(sum, (N / 2) * (N - 1));
  // This verifies that each element is deleted only once.
  for (int i = 0; i < N; i++) {
    ASSERT_EQ(result[i], 1);
  }
}

TEST(SharedPQTest, MultipleThreadInsertMultipleThreadDeleteWithDups) {
  SharedPQ<int, int, RELAXATION> pq;
  std::thread threads[NUM_THREADS];

  reset_variables();

  // Add elements using the multiple threads.
  for (int i = 0; i < NUM_THREADS; ++i) {
    threads[i] = std::thread(insert_helper<int, int, RELAXATION>, i,
                             std::ref(pq), 0, DATA_SIZE);
  }

  break_barrier(insert_barrier);

  // Wait for inserting threads to finish first.
  for (auto &th : threads) {
    th.join();
  }

  // Now verify delete_min as in case of single thread case.
  for (int i = 0; i < NUM_THREADS; ++i) {
    threads[i] =
        std::thread(delete_helper<int, int, RELAXATION>, i, std::ref(pq));
  }

  break_barrier(delete_barrier);

  while (ops < DATA_SIZE * NUM_THREADS) {
    // Wait until the queue is not empty.
  }

  stop_delete_min.store(true, std::memory_order_relaxed);

  for (auto &th : threads) {
    th.join();
  }

  ASSERT_EQ(ops, DATA_SIZE * NUM_THREADS);
  // Sum of series = N * N - 1 / 2
  int N = DATA_SIZE;
  // This verifies the checksum of data deleted.
  ASSERT_EQ(sum, NUM_THREADS * (N / 2) * (N - 1));
  // This verifies that each element is deleted only once.
  for (int i = 0; i < N; i++) {
    EXPECT_EQ(result[i], NUM_THREADS);
  }
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
