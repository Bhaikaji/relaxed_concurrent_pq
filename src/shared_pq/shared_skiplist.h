// Skiplist implementation according to the idea given by Herlihy and Shavit
// in the book "Art of Multiprocessor Programming".

#ifndef SHARED_SKIPLIST
#define SHARED_SKIPLIST

#include "../lib/generic_skiplist.h"
#include "../lib/item.h"
#include "../lib/logger.h"
#include <cassert>
#include <chrono>
#include <functional>
#include <random>

template <class K, class V, int Rlx>
class SharedSkipList : public SkipList<K, V> {
public:
  void insert(const K &key);
  void insert(const K &key, const V &value, int thread_id = 0);
  void insert(Item<K, V> *item, int thread_id = 0);

  bool erase_with_skipnode(skv *node_to_remove, int thread_id = 0);
  bool erase(const K &key, int thread_id = 0);

  bool find(Item<K, V> *item, std::vector<skv *> &predecessors,
            std::vector<skv *> &successors, int thread_id = 0);
  bool find(const K &key, std::vector<skv *> &predecessors,
            std::vector<skv *> &successors, int thread_id = 0);

  bool delete_min(V &value, int thread_id = 0);

  Item<K, V> *find_min(V &value, skv **peeked_node, int thread_id = 0);

  bool empty() const;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V, int Rlx>
void SharedSkipList<K, V, Rlx>::insert(const K &key) {
  insert(key, key);
}

template <class K, class V, int Rlx>
void SharedSkipList<K, V, Rlx>::insert(const K &key, const V &value,
                                       const int thread_id) {
  Item<K, V> *item = new Item<K, V>(key, value);
  insert(item, thread_id);
}

template <class K, class V, int Rlx>
void SharedSkipList<K, V, Rlx>::insert(Item<K, V> *item, int thread_id) {
  LOG(thread_id, "shared_skiplist", "insert", "Entering", item->key,
      item->value);
  const int bottom_level = 0;
  std::vector<skv *> predecessors(this->template max_level, nullptr);
  std::vector<skv *> successors(this->template max_level, nullptr);
  const int new_node_level = this->template random_level();
  skv *new_node_ptr = this->template make_node(item, new_node_level);

  while (true) {
    LOG(thread_id, "shared_skiplist", "insert",
        "check if item is already present", item->key, item->value,
        new_node_level);
    find(item, predecessors, successors, thread_id);

    // Note: Check is not done at this point on whether an item is present in
    // the SkipList or not. This is because we allow duplicate items in the
    // SkipList.
    for (int level = bottom_level; level < new_node_level; ++level) {
      skv *succ = successors[level];
      new_node_ptr->forward[level] = static_cast<skv *>(get_unmarked_ref(succ));
    }
    LOG(thread_id, "shared_skiplist", "insert", "linked to successor",
        item->key, item->value, new_node_level);
    // Once the bottom level is successfully linked, the node is logically
    // present in the list.
    skv *pred = predecessors[bottom_level];
    skv *succ = successors[bottom_level];
    if (!__sync_bool_compare_and_swap(
            &get_unmarked_ref(pred)->forward[bottom_level],
            get_unmarked_ref(succ), get_unmarked_ref(new_node_ptr))) {
      LOG(thread_id, "shared_skiplist", "insert",
          "CAS failed on bottom level, retry from beginning", item->key,
          item->value, new_node_level);
      continue;
    }
    LOG(thread_id, "shared_skiplist", "insert",
        "linked at bottom level, now move upwards", item->key, item->value,
        new_node_level);
    // Once the bottom level is done, work bottom-up to insert the new node in
    // upper levels.
    for (int level = bottom_level + 1; level < new_node_level; ++level) {
      while (true) {
        pred = predecessors[level];
        succ = successors[level];
        if (__sync_bool_compare_and_swap(
                &get_unmarked_ref(pred)->forward[level], get_unmarked_ref(succ),
                get_unmarked_ref(new_node_ptr))) {
          break;
        }
        LOG(thread_id, "shared_skiplist", "insert",
            "find again coz' CAS failed on level", level, item->key,
            item->value, new_node_level);
        find(item, predecessors, successors, thread_id);
      }
      LOG(thread_id, "shared_skiplist", "insert", "level done:", level,
          item->key, item->value, new_node_level);
    }
    // New node has been successfully linked at all levels, we can now return.
    LOG(thread_id, "shared_skiplist", "insert", "success!!", item->key,
        item->value, new_node_level);
    return;
  }
}

template <class K, class V, int Rlx>
bool SharedSkipList<K, V, Rlx>::find(Item<K, V> *item,
                                     std::vector<skv *> &predecessors,
                                     std::vector<skv *> &successors,
                                     int thread_id) {
  return find(item->key, predecessors, successors, thread_id);
}

template <class K, class V, int Rlx>
bool SharedSkipList<K, V, Rlx>::find(const K &key,
                                     std::vector<skv *> &predecessors,
                                     std::vector<skv *> &successors,
                                     int thread_id) {
  LOG(thread_id, "shared_skiplist", "find", key);
  const int bottom_level = 0;
  bool snip;

  skv *pred = nullptr;
  skv *cur = nullptr;
  skv *succ = nullptr;

retry:
  while (true) {
    pred = this->template head;
    for (int level = this->template node_level(this->template head) - 1;
         level >= bottom_level; --level) {
      LOG(thread_id, "shared_skiplist", "find at level", level, key);
      cur = get_unmarked_ref(pred)->forward[level];
      while (true) {
        succ = get_unmarked_ref(cur)->forward[level];
        while (is_marked_ref(succ)) {
          // Try to delete marked node physically on the way.
          LOG(thread_id, "shared_skiplist", "find", "deleting marked nodes!",
              key);
          snip = __sync_bool_compare_and_swap(
              &get_unmarked_ref(pred)->forward[level], get_unmarked_ref(cur),
              get_unmarked_ref(succ));
          if (!snip) {
            goto retry;
          }
          // Update cur and succ to point to the new nodes on the list.
          cur = get_unmarked_ref(pred)->forward[level];
          succ = get_unmarked_ref(cur)->forward[level];
        }
        if (get_unmarked_ref(cur)->item->key < key) {
          pred = cur;
          cur = succ;
        } else {
          break;
        }
      }
      predecessors[level] = pred;
      successors[level] = cur;
    }
    LOG(thread_id, "shared_skiplist", "find done", key);
    return get_unmarked_ref(cur)->item->key == key;
  }
}

template <class K, class V, int Rlx>
bool SharedSkipList<K, V, Rlx>::erase_with_skipnode(skv *node_to_remove,
                                                    int thread_id) {
  const int bottom_level = 0;
  skv *succ;
  // Attempt to mark all the nodes from top level all the way to
  // bottom_level + 1.
  for (int level =
           this->template node_level(get_unmarked_ref(node_to_remove)) - 1;
       level > bottom_level; --level) {
    // Attempt to logically delete by marking the forward reference using
    // CAS.
    succ = get_unmarked_ref(node_to_remove)->forward[level];
    while (!is_marked_ref(succ)) {
      __sync_bool_compare_and_swap(
          &get_unmarked_ref(node_to_remove)->forward[level],
          get_unmarked_ref(succ), get_marked_ref(succ));
      succ = get_unmarked_ref(node_to_remove)->forward[level];
    }
  }
  LOG(thread_id, "shared_skiplist", "erase",
      "All nodes except bottom level done",
      get_unmarked_ref(node_to_remove)->item->key);
  // Now mark the bottom_level reference of node_to_delete.
  succ = get_unmarked_ref(node_to_remove)->forward[bottom_level];
  while (true) {
    bool i_marked_it = __sync_bool_compare_and_swap(
        &get_unmarked_ref(node_to_remove)->forward[bottom_level],
        get_unmarked_ref(succ), get_marked_ref(succ));
    succ = get_unmarked_ref(node_to_remove)->forward[bottom_level];
    if (i_marked_it) {
      // Optimization.
      LOG(thread_id, "shared_skiplist", "erase",
          "Bottom level node marked, now calling an optimizing find()",
          get_unmarked_ref(node_to_remove)->item->key);
      std::vector<skv *> predecessors(this->template max_level, nullptr);
      std::vector<skv *> successors(this->template max_level, nullptr);
      find(get_unmarked_ref(node_to_remove)->item->key, predecessors,
           successors, thread_id);
      return true;
    } else if (is_marked_ref(succ)) {
      LOG(thread_id, "shared_skiplist", "erase",
          "Failed to mark, but someone else deleted it, so return false",
          get_unmarked_ref(node_to_remove)->item->key);
      return false;
    } else {
      LOG(thread_id, "shared_skiplist", "erase",
          "Failed to mark, but node is still unmarked - "
          "meaning the node changed",
          get_unmarked_ref(node_to_remove)->item->key);
      continue;
    }
  }
}

template <class K, class V, int Rlx>
bool SharedSkipList<K, V, Rlx>::erase(const K &key, int thread_id) {
  LOG(thread_id, "shared_skiplist", "erase", "Entering...", key);
  const int bottom_level = 0;
  std::vector<skv *> predecessors(this->template max_level, nullptr);
  std::vector<skv *> successors(this->template max_level, nullptr);
  skv *succ;
  bool found = find(key, predecessors, successors, thread_id);
  if (!found) {
    LOG(thread_id, "shared_skiplist", "erase", key, "not found so return.",
        key);
    return false;
  }
  LOG(thread_id, "shared_skiplist", "erase", "found, so delete it now.", key);
  skv *node_to_remove = successors[bottom_level];
  return erase_with_skipnode(node_to_remove, thread_id);
}

template <class K, class V, int Rlx>
bool SharedSkipList<K, V, Rlx>::delete_min(V &value, int thread_id) {
  // Call find_min to find (maybe) unmarked item in the list. Make it ready for
  // deletion by marking the forward references.
  LOG(thread_id, "shared_skiplist", "delete_min", "Entering...");
  skv *peeked_node;
  auto item = find_min(value, &peeked_node, thread_id);
  if (item != nullptr) {
    if (!item->taken.exchange(true, std::memory_order_relaxed)) {
      // Do not attemp to erase here itself. Leave the work to find_min.
      return erase_with_skipnode(peeked_node, thread_id);
    } else {
      item = find_min(value, &peeked_node, thread_id);
    }
  } else {
    return false;
  }
  // Retry once if unsuccessful.
  if (item && erase_with_skipnode(peeked_node, thread_id)) {
    LOG(thread_id, "shared_skiplist", "delete_min", "item successfully deleted",
        item->key);
    return true;
  } else {
    return false;
  }
}

template <class K, class V, int Rlx>
Item<K, V> *SharedSkipList<K, V, Rlx>::find_min(V &value, skv **peeked_node,
                                                int thread_id) {
  LOG(thread_id, "shared_skiplsit", "find_min", "Entering");
  const int bottom_level = 0;
  unsigned int skip_count = Rlx;
  unsigned int max_attempt = 2;

  do {
    --max_attempt;
    // Start at head.
    skv *cur = this->template head->forward[bottom_level];
    // Generate a random number in range [0, skip_count]. It is the count of
    // elements that will be skipped.
    int nelements_to_skip = rand() % skip_count;

    // Reset skip_count to 0.
    skip_count = 0;
    while (get_unmarked_ref(cur) != this->template NIL) {
      if (skip_count < nelements_to_skip) {
        cur = get_unmarked_ref(cur)->forward[bottom_level];
        skip_count++;
      } else {
        if (get_unmarked_ref(cur)->item->taken.load(
                std::memory_order_relaxed)) {
          cur = get_unmarked_ref(cur)->forward[bottom_level];
        } else {
          *peeked_node = cur;
          value = get_unmarked_ref(cur)->item->value;
          return get_unmarked_ref(cur)->item;
        }
      }
    }
  } while (skip_count > 0 && max_attempt > 0);

  return nullptr;
}

template <class K, class V, int Rlx>
bool SharedSkipList<K, V, Rlx>::empty() const {
  return this->template head->forward[0] == this->template NIL;
};

#endif // SHARED_SKIPLIST
