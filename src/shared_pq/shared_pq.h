// SharedPQ is a non-blocking priority queue based on Non-blocking Skiplists.

#ifndef SHARED_PQ
#define SHARED_PQ

#include "../lib/item.h"
#include "../lib/pq.h"
#include "shared_skiplist.h"

template <class K, class V, int Rlx> class CombinedNBPQ;

template <class K, class V, int Rlx> class SharedPQ : public PQ<K, V, Rlx> {
  friend class CombinedNBPQ<K, V, Rlx>;

public:
  void insert(const K &key) override;

  void insert(const K &key, const V &value) override;

  bool delete_min(V &value) override;

  Item<K, V> *find_min(V &value, skv **peeked_node);

  void print() const;

  bool empty() const;

private:
  SharedSkipList<K, V, Rlx> shared_sl;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V, int Rlx>
void SharedPQ<K, V, Rlx>::insert(const K &key) {
  insert(key, key);
}

template <class K, class V, int Rlx>
void SharedPQ<K, V, Rlx>::insert(const K &key, const V &value) {
  shared_sl.insert(key, value);
}

template <class K, class V, int Rlx>
bool SharedPQ<K, V, Rlx>::delete_min(V &value) {
  return shared_sl.delete_min(value);
}

template <class K, class V, int Rlx>
Item<K, V> *SharedPQ<K, V, Rlx>::find_min(V &value, skv **peeked_node) {
  return shared_sl.find_min(value, peeked_node);
}

template <class K, class V, int Rlx> void SharedPQ<K, V, Rlx>::print() const {
  shared_sl.print();
}

template <class K, class V, int Rlx> bool SharedPQ<K, V, Rlx>::empty() const {
  return shared_sl.empty();
}

#endif
