// CombinedPQ is a priority which is internally composed of two priority queues
// internally. One of the priority queues involved is DistributedPQ which
// ensures low contention but low orderig gurantee. The other priority queue
// involved is SharedPQ. It ensures high ordering guarantee but at the cost of
// high contention. So a combination of the two priority queues yields a queue
// with relatively low contention and good ordering gurantee. These properties
// of the priority queue can be adjusted by a variable 'k' denoted by Rlx in
// code. So with the variable, the ordering gurantee for the CombinedPQ is
// `roh` = Tk, where T = number of threads concurrently accessing the PQ.

#ifndef COMBINED_NB_PQ
#define COMBINED_NB_PQ

#include "../dist_pq/dist_pq.h"
#include "../shared_pq/shared_pq.h"

template <class K, class V, int Rlx> class CombinedNBPQ : public PQ<K, V, Rlx> {
public:
  void insert(const K &key) override;

  void insert(const K &key, const V &value) override;

  bool delete_min(V &value) override;

  void print() const;

private:
  DistributedPriorityQueue<K, V, Rlx, SharedPQ<K, V, Rlx>> dist_pq;
  SharedPQ<K, V, Rlx> shared_pq;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V, int Rlx>
void CombinedNBPQ<K, V, Rlx>::insert(const K &key) {
  insert(key, key);
}

template <class K, class V, int Rlx>
void CombinedNBPQ<K, V, Rlx>::insert(const K &key, const V &value) {
  dist_pq.insert(key, value, &shared_pq);
}

// (TODO) Find best values from both dist_pq and shared_pq and return the best
// of the two. If both of them turn out to be empty, try spy on other threads'
// dist_pq.
// Problem with the implementation is that the delete_min operation can return
// false even if there are elements present in the PQ, the reason again being
// the concurrent threads: if the best item found out by the find_min of dist_pq
// is concurrently marked by another thread, the CAS fails and hence delete_min
// returns false. So the caller should keep in mind that it doesn't mean that
// PQ is empty if it's delete_min returns false.
template <class K, class V, int Rlx>
bool CombinedNBPQ<K, V, Rlx>::delete_min(V &value) {
  V dist_value, shared_value;

  do {
    Item<K, V> *dist_item = dist_pq.find_min(dist_value);
    skv *peeked_node;
    Item<K, V> *shared_item = shared_pq.find_min(shared_value, &peeked_node);

    if (dist_item != nullptr && shared_item != nullptr) {
      if (dist_value <= shared_value) {
        value = dist_value;
        return !dist_item->taken.exchange(true);
      } else {
        value = shared_value;
        if (!shared_item->taken.exchange(true, std::memory_order_relaxed)) {
          return shared_pq.shared_sl.erase_with_skipnode(peeked_node);
        }
      }
    }
    if (dist_item != nullptr) {
      value = dist_value;
      return !dist_item->taken.exchange(true);
    }

    if (shared_item != nullptr) {
      value = shared_value;
      if (!shared_item->taken.exchange(true, std::memory_order_relaxed)) {
        return shared_pq.shared_sl.erase_with_skipnode(peeked_node);
      }
    }
  } while (dist_pq.spy() > 0);

  return false;
}

template <class K, class V, int Rlx>
void CombinedNBPQ<K, V, Rlx>::print() const {
  LOG2("SharedPQ");
  shared_pq.print();
}

#endif // COMBINED_NB_PQ
