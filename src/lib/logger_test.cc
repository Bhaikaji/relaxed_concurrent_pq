#include "logger.h"

#include <thread>

void print_helper() {
  for (int i = 0; i < 10; i++) {
    LOG("my value is ", i);
  }
}

int main(void) {
  std::thread threads[10];
  
  for (int i = 0; i < 10; i++) {
    threads[i] = std::thread(print_helper);
  }

  for (auto& th : threads) {
    th.join();
  }

  return 0;
}
