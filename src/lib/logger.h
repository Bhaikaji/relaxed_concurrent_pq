#ifndef LOGGER_H
#define LOGGER_H

#include "log.h"

static Logger log_inst(false);
static Logger log_inst2(true);

#define LOG log_inst.log
#define LOG2 log_inst2.log

#endif // LOGGER_H
