#ifndef ITEM_H
#define ITEM_H

#include <atomic>

template <class K, class V> struct Item {
  K key;
  V value;
  // The first thread that successfully choses this item for its delete-min
  // operation sets this flag to true atomically. Once taken, an item is 
  // logically deleted.
  std::atomic_bool taken = ATOMIC_FLAG_INIT;

  Item(const K &key) { Item(key, key); }
  Item(const K &key, const V &value) : key(key), value(value) {}
};

#endif // ITEM_H
