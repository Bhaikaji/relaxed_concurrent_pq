#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <iostream>
#include <mutex>

class Logger {
public:
  Logger(bool enable = true);

  template <typename... Args> void log(Args &&... args);

private:
  template <typename T> void log_impl(T &&arg);
  template <typename T, typename... Args>
  void log_impl(T &&arg, Args &&... args);

private:
  std::mutex m;
  bool enable;
};

//
// Implementation details below. Clients should ignore it.
//

template <typename... Args> void Logger::log(Args &&... args) {
  if (!enable) {
    return;
  }
  m.lock();
  log_impl(std::forward<Args>(args)...);
  m.unlock();
}

template <typename T> void Logger::log_impl(T &&arg) {
  std::cout << std::forward<T>(arg) << std::endl;
}

template <typename T, typename... Args>
void Logger::log_impl(T &&arg, Args &&... args) {
  std::cout << std::forward<T>(arg) << " ";
  log_impl(std::forward<Args>(args)...);
}
#endif // LOG_H_INCLUDED
