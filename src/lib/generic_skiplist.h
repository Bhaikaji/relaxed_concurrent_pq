#ifndef GENERIC_SKIPLIST_H
#define GENERIC_SKIPLIST_H

#include "../lib/item.h"
#include <ctime>
#include <iostream>
#include <limits>
#include <vector>

#define get_marked_ref(_p) (reinterpret_cast<skv *>(((uintptr_t)(_p)) | 1))
#define get_unmarked_ref(_p) (reinterpret_cast<skv *>(((uintptr_t)(_p)) & ~1))
#define is_marked_ref(_p) (((uintptr_t)(_p)) & 1)

template <class K, class V> struct SkipNode {
  Item<K, V> *item;
  std::vector<SkipNode *> forward;
  SkipNode(Item<K, V> *item, int level) : item(item), forward(level, nullptr) {}
};

#define skv SkipNode<K, V>

template <class K, class V> class SkipList {
public:
  SkipList();
  virtual ~SkipList();

  void print() const;
  bool find(const K &key, std::vector<skv *> &predecessors) const;

protected:
  // Generates node levels in the range [1, max_level].
  int random_level() const;

  // Returns the maximum height of a node.
  static int node_level(const skv *v);

  // Creates a node on the heap and returns a pointer to it.
  static skv *make_node(Item<K, V> *item, int level);

  const float probability;
  const int max_level;
  skv *head;
  skv *NIL;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V>
SkipList<K, V>::SkipList() : probability(0.5), max_level(16) {
  auto head_key = std::numeric_limits<int>::min();
  head = new skv(new Item<K, V>(head_key, head_key), max_level);

  auto nil_key = std::numeric_limits<int>::max();
  NIL = new skv(new Item<K, V>(nil_key, nil_key), max_level);

  std::fill(head->forward.begin(), head->forward.end(), NIL);

  // Initialize the random number generator.
  srand(std::time(0));
}

template <class K, class V> SkipList<K, V>::~SkipList() {
  auto node = head;
  while (get_unmarked_ref(node)->forward[0]) {
    auto tmp = node;
    node = get_unmarked_ref(node)->forward[0];
    // TODO (ashokadhikari) An attempt to delete an item here may cause
    // segmentation fault because the current item may be an item spied from
    // another thread. As such, the ownership of the item is not with this
    // thread and hence the thread should not try to delete it. Worst of all
    // it may already have been deleted by the other thread.
    // MEMORY LEAK GURANTEED!!
    // if (tmp->item)
    // delete tmp->item;
    delete get_unmarked_ref(tmp);
  }
  delete NIL;
}

template <class K, class V> void SkipList<K, V>::print() const {
  skv *cur = head->forward[0];
  std::cout << "{" << std::endl;
  while (get_unmarked_ref(cur) != NIL) {
    std::cout << "value: " << get_unmarked_ref(cur)->item->value
              << ", key: " << get_unmarked_ref(cur)->item->key
              << ", level: " << node_level(get_unmarked_ref(cur))
              << ", taken: " << get_unmarked_ref(cur)->item->taken;
    cur = get_unmarked_ref(cur)->forward[0];
    if (get_unmarked_ref(cur) != NIL)
      std::cout << " : ";
    std::cout << "\n";
  }
  std::cout << "}" << std::endl;
}

template <class K, class V>
bool SkipList<K, V>::find(const K &key,
                          std::vector<skv *> &predecessors) const {
  skv *pred = head;
  skv *cur = nullptr;
  skv *succ = nullptr;

  for (unsigned int level = node_level(head); level-- > 0;) {
    cur = pred->forward[level];
    while (true) {
      succ = cur->forward[level];
      while (cur->item->taken) {
        // Physical removal from this 'level'.
        pred->forward[level] = succ;
        cur = succ;
        succ = cur->forward[level];
      }
      if (cur->item->key < key) {
        pred = cur;
        cur = succ;
      } else {
        break;
      }
    }
    predecessors[level] = pred;
  }
  return cur->item->key == key;
}

template <class K, class V>
int SkipList<K, V>::node_level(const SkipNode<K, V> *v) {
  return v->forward.size();
}

template <class K, class V>
SkipNode<K, V> *SkipList<K, V>::make_node(Item<K, V> *item, int level) {
  return new SkipNode<K, V>(item, level);
}

template <class K, class V> int SkipList<K, V>::random_level() const {
  int v = 1;
  while (((double)rand() / RAND_MAX) < probability && v < max_level) {
    v++;
  }
  return v;
}

#endif // GENERIC_SKIPLIST_H
