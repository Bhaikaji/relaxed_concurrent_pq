//
// Created by Ashok Adhikari on 4/5/17.
//
// A Priority Queue Interface to be used by all the Priority Queues.

#ifndef PQ_PQ_H
#define PQ_PQ_H

template <class K, class V, int Rlx> class PQ {
public:
  virtual ~PQ() {}

  virtual void insert(const K &key) = 0;

  virtual void insert(const K &key, const K &value) = 0;

  virtual bool delete_min(V &value) = 0;
};

#endif // PQ_PQ_H
