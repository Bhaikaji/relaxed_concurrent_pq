#include "combined_pq.h"

#include "../lib/logger.h"
#include <chrono>
#include <gtest/gtest.h>
#include <iostream>
#include <list>
#include <mutex>

const int NUM_THREADS = 10;
const int DATA_SIZE = 100;

const int RELAXATION = 10;

static std::atomic<int> sum(0);
static std::vector<int> result(DATA_SIZE *NUM_THREADS);
static std::atomic<int> delete_min_counter(0);

int relaxed_upper_bounds(const int iteration, const int relaxation) {
  return NUM_THREADS * relaxation + iteration;
}

template <class K, class V, int Rlx>
void insert_helper(CombinedPQ<K, V, Rlx> &cpq, int start, int count) {
  for (int i = start; i < start + count; i++) {
    cpq.insert(i, i);
  }
  for (int i = 0; i < DATA_SIZE * count; i++) {
    int value;
    if (!cpq.delete_min(value)) {
      continue;
    }
    ASSERT_LE(value, relaxed_upper_bounds(delete_min_counter, RELAXATION));
    delete_min_counter.fetch_add(1, std::memory_order_relaxed);
    sum.fetch_add(value, std::memory_order_relaxed);
    result[value]++;
  }
}

TEST(CombinedPQTest, VerifyRelaxationBounds) {
  CombinedPQ<int, int, RELAXATION> cpq;
  std::thread threads[NUM_THREADS];

  for (int i = 0; i < NUM_THREADS; i++) {
    threads[i] = std::thread(insert_helper<int, int, RELAXATION>, std::ref(cpq),
                             i * DATA_SIZE, DATA_SIZE);
  }
  for (auto &th : threads) {
    th.join();
  }
  // Sum of series = N * N - 1 / 2
  int N = NUM_THREADS * DATA_SIZE;
  ASSERT_EQ(sum, (N / 2) * (N - 1));
  for (int i = 0; i < NUM_THREADS * DATA_SIZE; i++) {
    ASSERT_EQ(result[i], 1);
  }
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
