#ifndef SKIPLIST_H
#define SKIPLIST_H

#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

class SkipList {
public:
  SkipList();
  ~SkipList();

  // It prints the key, value, level of each node of the skip list.
  void print();

  // It searches the skip list and returns the element corresponding to the
  // search_key; otherwise it returns failure, in
  // the form of null pointer.
  int find(int search_key);

  // It searches the skip list for elements with search_key, if there is an
  // element with that key its value is reassigned to the new_value, other-
  // wise it creates and splices a new node, of random level.
  void insert(int search_key, const int new_value);

  // It deletes the element containing search_key, if it exists.
  void erase(int search_key);

private:
  struct SkipNode {
    int key;
    int value;

    // Pointers to successor nodes.
    std::vector<SkipNode *> forward;

    SkipNode(int k, const int v, int level)
        : key(k), value(v), forward(level, nullptr) {}
  };

  // Generates node levels in the range [1, max_level);
  int random_level() const;

  // Returns number of incoming and outgoing pointers.
  static int node_level(const SkipNode *v);

  // Creates a node on the heap and returns a pointer to it.
  static SkipNode *make_node(int key, int val, int level);

  // Returns the first node for which node->key < search_key is false.
  // i.e. the node with with maximum key lesser than search_key
  SkipNode *lower_bound(int search_key) const;

  // Returns a collection of pointers to Nodes. result[i] holds the last
  // node of level i + 1 for which result[i]->key < search_key.
  std::vector<SkipNode *> predecessors(int search_key) const;

  // Lock for the whole SkipList operations.
  std::mutex skip_lock;
  // Probability that each element grows to next level.
  const float probability;
  // Maximum allowed level of the SkipList.
  const int max_level;
  SkipNode *head;
  SkipNode *NIL;
};

#endif // SKIPLIST_H
