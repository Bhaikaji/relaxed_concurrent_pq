#include "skiplist.h"

#include <gtest/gtest.h>
#include <iostream>

void insert_helper(SkipList *s, int count) {
  for (int i = 0; i < count; i++) {
    s->insert(i, i);
  }  
}

void delete_helper(SkipList* s, int count) {
  for (int i = 0; i < count; i++) {
    s->erase(i);
  }  
}

TEST(SkipListTest, MultipleThreadsInsertTest) {
  SkipList s;

  const int num_threads = 10;
  const int count = 100;
  std::thread threads[num_threads];

  for (int i = 0; i < num_threads; i++) {
    threads[i] = std::thread(insert_helper, &s, count);
  }

  for (std::thread& th: threads) {
    th.join();
  }

  s.print();
  for (int i = 0; i < count; i++) {
    ASSERT_EQ(s.find(i), i);
    s.erase(i);
    ASSERT_EQ(s.find(i), -1);
  }
}

TEST(SkipListTest, MultipleThreadsInsertDeleteTest) {
  SkipList s;

  const int num_threads = 10;
  const int count = 100;
  std::thread threads[num_threads];

  for (int i = 0; i < num_threads; i++) {
    threads[i] = std::thread(insert_helper, &s, count);
  }

  for (std::thread& th: threads) {
    th.join();
  }

  s.print();

  // now delete using multiple threads
  for (int i = 0; i < num_threads; i++) {
    threads[i] = std::thread(delete_helper, &s, count);
  } 

  for (int i = 0; i < count; i++) {
    ASSERT_EQ(s.find(i), -1);
  }

  for (std::thread& th: threads) {
    th.join();
  }
}

int main (int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
