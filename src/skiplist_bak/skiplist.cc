#include "skiplist.h"

SkipList::SkipList() : probability(0.5), max_level(16) {
  int head_key = std::numeric_limits<int>::min();
  head = new SkipNode(head_key, head_key, max_level);

  int nil_key = std::numeric_limits<int>::max();
  NIL = new SkipNode(nil_key, nil_key, max_level);

  std::fill(head->forward.begin(), head->forward.end(), NIL);
}

SkipList::~SkipList() {
  auto node = head;
  while (node->forward[0]) {
    auto tmp = node;
    node = node->forward[0];
    delete tmp;
  }
  delete node;
}

int SkipList::find(int search_key) {
  skip_lock.lock();
  // TODO: Assuming that the skiplist doesn't have keys with negative values.
  if (auto x = lower_bound(search_key)) {
    if (x->key == search_key && x != NIL) {
      skip_lock.unlock();
      return x->value;
    }
  }
  skip_lock.unlock();
  return -1;
}

void SkipList::print() {
  skip_lock.lock();
  SkipNode *list = head->forward[0];
  std::cout << "{" << std::endl;

  while (list != NIL) {
    std::cout << "value: " << list->value << ", key: " << list->key
              << ", level: " << node_level(list);

    list = list->forward[0];

    if (list != NIL)
      std::cout << " : ";

    std::cout << "\n";
  }
  std::cout << "}" << std::endl;
  skip_lock.unlock();
}

void SkipList::insert(int search_key, const int new_value) {
  skip_lock.lock();
  auto preds = predecessors(search_key);

  auto next = preds[0]->forward[0];
  if (next->key == search_key && next != NIL) {
    next->value = new_value;
    skip_lock.unlock();
    return;
  }

  // Create a new node.
  const int new_node_level = random_level();
  auto new_node_ptr = make_node(search_key, new_value, new_node_level);

  // Connect pointers of predecessors to new node and then to successors.
  for (int i = 0; i < new_node_level; ++i) {
    new_node_ptr->forward[i] = preds[i]->forward[i];
    preds[i]->forward[i] = new_node_ptr;
  }
  skip_lock.unlock();
}

void SkipList::erase(int search_key) {
  skip_lock.lock();
  auto preds = predecessors(search_key);

  // Check if the node exists.
  auto node = preds[0]->forward[0];
  if (node->key != search_key || node == NIL) {
    skip_lock.unlock();
    return;
  }

  // Update pointers and delete node.
  for (int i = 0; i < node_level(node); ++i) {
    preds[i]->forward[i] = node->forward[i];
  }
  delete node;
  skip_lock.unlock();
}

int SkipList::node_level(const SkipNode *v) { return v->forward.size(); }

SkipList::SkipNode *SkipList::make_node(int key, int val, int level) {
  return new SkipNode(key, val, level);
}

int SkipList::random_level() const {
  int v = 1;
  while (((double)std::rand() / RAND_MAX) < probability && v < max_level) {
    v++;
  }
  return v;
}

SkipList::SkipNode *SkipList::lower_bound(int search_key) const {
  SkipNode *x = head;

  for (unsigned int i = node_level(head); i-- > 0;) {
    while (x->forward[i]->key < search_key) {
      x = x->forward[i];
    }
  }

  return x->forward[0];
}

std::vector<SkipList::SkipNode *> SkipList::predecessors(int search_key) const {
  std::vector<SkipNode *> result(node_level(head), nullptr);
  SkipNode *x = head;

  for (unsigned int i = node_level(head); i-- > 0;) {
    while (x->forward[i]->key < search_key) {
      x = x->forward[i];
    }
    result[i] = x;
  }
  return result;
}
