// It is similar to a normal sequential implementation of a skiplist,
// with the only difference that each SkipNode has a reference to an
// item instead value of the item. Further, each DistributedSkiplist also
// mantains a list of items that can be spied upon by other processes.

#ifndef DISTRIBUTED_SKIPLIST_H
#define DISTRIBUTED_SKIPLIST_H

#include <atomic>
#include <list>
#include <thread>
#include <vector>

#include "../lib/generic_skiplist.h"
#include "../lib/item.h"
#include "../lib/logger.h"
#include "utils/thread_local_ptr.h"

template <class K, class V, int Rlx, class PQ> class DistributedPriorityQueue;

template <class K, class V, int Rlx, class PQ>
class DistributedSkipList : public SkipList<K, V> {
public:
  ~DistributedSkipList() {}

  // Inserts an node with key item->key and value item->value. If Item is
  // provided, then a SkipNode with a reference to the provided Item is
  // created.
  void insert(const K &key);
  void insert(const K &key, const V &value);
  void insert(const K &key, const V &value, PQ *shared_pq);
  void insert(Item<K, V> *item);

  // Deletes the  node with key.
  virtual void erase(const K &key);

  // Marks the first unmarked node in the skiplist and returns its value in
  // 'value'.
  bool delete_min(V &value, DistributedPriorityQueue<K, V, Rlx, PQ> *parent);

  // Returns the minimum element found, false if no element present.
  Item<K, V> *find_min(V &value);

  // Prints the values of the skiplist.
  void print_item_list() const;

  // Access to the item_list.
  std::list<Item<K, V> *> *get_item_list();
  // Returns the number of elements that are not 'taken'.
  unsigned long get_item_list_size() const;

  // Returns id of the current thread.
  int get_int_thread_id() const;
  void set_int_thread_id(int id) { thread_id = id; }

  int spy(DistributedPriorityQueue<K, V, Rlx, PQ> *parent);
  int spy(DistributedSkipList<K, V, Rlx, PQ> *victim);

private:
  int thread_id = 0;
  std::list<Item<K, V> *> item_list;
  unsigned long queue_size = 0;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V, int Rlx, class PQ>
void DistributedSkipList<K, V, Rlx, PQ>::insert(Item<K, V> *item) {
  std::vector<skv *> preds(this->template node_level(this->template head));
  this->template find(item->key, preds);

  // Create a new item.
  const int new_node_level = this->template random_level();
  auto new_node_ptr = this->template make_node(item, new_node_level);

  // Connect pointers of predecessors to new node and then to successors.
  for (int i = 0; i < new_node_level; ++i) {
    new_node_ptr->forward[i] = preds[i]->forward[i];
    preds[i]->forward[i] = new_node_ptr;
  }
  ++queue_size;
}

template <class K, class V, int Rlx, class PQ>
void DistributedSkipList<K, V, Rlx, PQ>::insert(const K &key) {
  insert(key, key);
}

template <class K, class V, int Rlx, class PQ>
void DistributedSkipList<K, V, Rlx, PQ>::insert(const K &key, const V &value) {
  insert(key, value, nullptr);
}

template <class K, class V, int Rlx, class PQ>
void DistributedSkipList<K, V, Rlx, PQ>::insert(const K &key, const V &value,
                                                PQ *shared_pq) {
  if (shared_pq != nullptr && get_item_list_size() >= Rlx) {
    shared_pq->insert(key, value);
  } else {
    Item<K, V> *item = new Item<K, V>(key, value);
    insert(item);
    item_list.push_back(item);
  }
}

template <class K, class V, int Rlx, class PQ>
void DistributedSkipList<K, V, Rlx, PQ>::erase(const K &key) {
  std::vector<skv *> preds(this->template node_level(this->template head));

  if (!this->template find(key, preds)) {
    return;
  }

  auto node = preds[0]->forward[0];

  // Update pointers and delete node.
  for (int i = 0; i < this->template node_level(node); ++i) {
    preds[i]->forward[i] = node->forward[i];
  }
  // This problem can be solved using a pool of nodes. Pool will have the
  // ownership of all the nodes. Every call to make_node is fulfilled by the
  // pool and every delete returns the node back to the pool. Hence, even when
  // a node is removed from the SkipList, we never encounter a null pointer
  // exception because it is there even though inactive.
  // TODO (ashokadhikari) Consider deleting the item here itself, instead of
  // delegeting it to the destructor. Problem is if we delete here, then the
  // following scenario can happen:
  // 1. t1 spied 10 items from t2.
  // 2. t1 calls delete_min. It is on process of removing the first item, x.
  //    It has reached a point where the current->item == nullptr check is
  //    done and it evaluates to false. So it is about to evaluate the second
  //    part of the if statement current->item->taken.
  //    occurs.
  // 3. t2 resumes, deletes the node, has one more call of delete_min, hence
  //    the node as well as item are removed.
  // 4. t1 resumes, tries current->item->taken but it is a segmentation fault,
  //    because item is already deleted by thread t2.
  //
  // This will not remove an item from the other list because it doesn't have
  // its access. So some synchronization in the delete_min method should do the
  // work. This is a place where memory leak occurs.
  const K v = node->item->value;
  // item_list.remove(node->item); // TODO (Memory leak!!!)
  delete node;
  LOG(thread_id, "dist_skiplist", "erase",
      "node deleted and item removed from list", v);
}

// Uses find_min to find the first unmarked item in the skiplist. If
// find_min returns nullptr meaning no item in skiplist, then a spy is
// performed to copy items from other threads. If however an item that is
// returned by the find_min is concurrently marked by another thread,
// delete_min returns false.
// TODO (ashokadhikari) One problem with the implementation is that we the
// delete_min function can return nullptr even if the PQ is not empty. This is
// the case when an item is in parallel marked by another thread. Analysis
// required on whether this affects the functionality of the PQ. The fact that
// we have a size() method which returns the current size of the PQ can helps
// on determining whether the PQ is empty or not. So, instead of the success of
// delete_min operation, the size() method should be relied upon to determine
// if the PQ is empty.
template <class K, class V, int Rlx, class PQ>
bool DistributedSkipList<K, V, Rlx, PQ>::delete_min(
    V &value, DistributedPriorityQueue<K, V, Rlx, PQ> *parent) {
  LOG(thread_id, "dist_skiplist", "delete_min", "Entering...");

  auto item = find_min(value);
  if (item == nullptr && spy(parent) > 0) {
    // Retry once after a successful spy().
    item = find_min(value);
  }
  if (item == nullptr) {
    // Give up now.
    return false;
  }
  bool success = !item->taken.exchange(true);
  // This means the node is successfully deleted, hence we decrease the size
  // of the queue.
  if (!success) {
    return false;
  }
  --queue_size;
  // Optimizing find. The side-effect of find will physically delete the node.
  std::vector<skv *> preds(this->template node_level(this->template head));
  this->template find(item->key, preds);
  return success;
}

template <class K, class V, int Rlx, class PQ>
Item<K, V> *DistributedSkipList<K, V, Rlx, PQ>::find_min(V &value) {
  LOG(thread_id, "dist_skiplist", "find_min", "Entering...");
  SkipNode<K, V> *current = this->template head->forward[0];

  while (current != this->template NIL) {
    if (current->item == nullptr || current->item->taken) {
      // We don't erase the marked items here.
      current = current->forward[0];
    } else {
      value = current->item->value;
      return current->item;
    }
  }
  return nullptr;
}

template <class K, class V, int Rlx, class PQ>
int DistributedSkipList<K, V, Rlx, PQ>::spy(
    DistributedPriorityQueue<K, V, Rlx, PQ> *parent) {
  LOG(thread_id, "dist_skiplist", "spy", "Entering...");
  const size_t num_threads = parent->dist_sl.num_threads();
  const size_t current_thread = parent->dist_sl.current_thread();

  if (num_threads < 2) {
    return 0;
  }

  size_t victim_index = std::rand() % num_threads;
  LOG(thread_id, "dist_skiplist", "spy", "Initial victim index", victim_index);
  if (victim_index == current_thread) {
    victim_index = (victim_index + 1) % num_threads;
  }
//  LOG(thread_id, "dist_skiplist", "spy", "Final victim index", victim_index);
  return spy(parent->dist_sl.get(victim_index));
}

template <class K, class V, int Rlx, class PQ>
int DistributedSkipList<K, V, Rlx, PQ>::spy(
    DistributedSkipList<K, V, Rlx, PQ> *victim) {
  LOG(thread_id, "dist_skiplist", "spyII", "Entering..");

  int count = 0;
  for (Item<K, V> *item : *victim->get_item_list()) {
    // Check for whether an item is null or not is required because an item
    // may be deleted by the owner thread while this thread is going through
    // its item list.
    if (item && !item->taken) {
      LOG(thread_id, "dist_skiplist", "spyII",
          "Spy successful! Taking item: value", item->value);
      insert(item);
      if ((count++/2) > Rlx) {
        break;
      }
    }
  }
  LOG(thread_id, "dist_skiplist", "spyII", "Spying finished Count spied",
      count);
  return count;
}

template <class K, class V, int Rlx, class PQ>
void DistributedSkipList<K, V, Rlx, PQ>::print_item_list() const {
  std::cout << std::endl << "Item List : HEAD -> ";
  SkipNode<K, V> *current = this->template head->forward[0];

  while (current != this->template NIL) {
    std::cout << current->item->key << " -> ";
    current = current->forward[0];
  }
  std::cout << "NIL" << std::endl;
}

template <class K, class V, int Rlx, class PQ>
std::list<Item<K, V> *> *DistributedSkipList<K, V, Rlx, PQ>::get_item_list() {
  return &item_list;
}

template <class K, class V, int Rlx, class PQ>
unsigned long DistributedSkipList<K, V, Rlx, PQ>::get_item_list_size() const {
  return queue_size;
//  int count = 0;
//
//  for (auto &item : item_list) {
//    if (!item->taken) {
//      count++;
//    }
//  }
//  return count;
}

template <class K, class V, int Rlx, class PQ>
int DistributedSkipList<K, V, Rlx, PQ>::get_int_thread_id() const {
  return thread_id;
}

#endif // DISTRIBUTED_SKIPLIST_H
