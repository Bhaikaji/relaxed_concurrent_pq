// A dist_pq is a distrubited priority queue.
// This concurrent priority queue allows for maximum concurrency with
// relatively low synchronization overhead. It uses a SkipList as an
// underlying priority queue. Inserts are done as usual, but when a
// delete-min operation finds that the priority queue is empty, it
// spies on elements from randomly selected victim. This also helps
// distributing tasks throughout the system.

#ifndef DISTRIBUTED_PRIORITY_QUEUE
#define DISTRIBUTED_PRIORITY_QUEUE

#include <list>

#include "../lib/pq.h"
#include "dist_skiplist.h"
#include "utils/thread_local_ptr.h"

template <class K, class V, int Rlx, class PqType>
class DistributedPriorityQueue : public PQ<K, V, Rlx> {
  friend int DistributedSkipList<K, V, Rlx, PqType>::spy(
      DistributedPriorityQueue<K, V, Rlx, PqType> *parent);

public:
  DistributedPriorityQueue() {}

  ~DistributedPriorityQueue(){};

  // Inserts item with key, key to the priority queue.
  void insert(const K &key) override;

  // Inserts item with key, value to the priority queue.
  void insert(const K &key, const V &value) override;

  void insert(const K &key, const V &value, PqType *shared_pq);

  void insert(Item<K, V> *item);

  // Removes the value with highest priority from the queue and returns its
  // value in 'value'.
  bool delete_min(V &value) override;

  Item<K, V> *find_min(V &value);

  int spy();

  // Returns the size of the priority queue associated with this thread.
  int size();

  // TODO (ashokadhikari) Remove these functions from the API.
  // This method is for DEBUG purposes and won't be present in RELEASE.
  //
  // Returns the id of the SkipList associated with this thread. ID is the id
  // of the thread owning this SkipList.
  int get_sl_id() const;

  void set_sl_id();

  void print() const;

private:
  kpq::thread_local_ptr<DistributedSkipList<K, V, Rlx, PqType>> dist_sl;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V, int Rlx, class PqType>
void DistributedPriorityQueue<K, V, Rlx, PqType>::insert(const K &key) {
  insert(key, key);
}

template <class K, class V, int Rlx, class PqType>
void DistributedPriorityQueue<K, V, Rlx, PqType>::insert(const K &key,
                                                         const V &value) {
  insert(key, value, nullptr);
}

template <class K, class V, int Rlx, class PqType>
void DistributedPriorityQueue<K, V, Rlx, PqType>::insert(const K &key,
                                                         const V &value,
                                                         PqType *shared_pq) {
  dist_sl.get()->insert(key, value, shared_pq);
}

template <class K, class V, int Rlx, class PqType>
void DistributedPriorityQueue<K, V, Rlx, PqType>::insert(Item<K, V> *item) {
  dist_sl.get()->insert(item);
}

template <class K, class V, int Rlx, class PqType>
bool DistributedPriorityQueue<K, V, Rlx, PqType>::delete_min(V &value) {
  return dist_sl.get()->delete_min(value, this);
}

template <class K, class V, int Rlx, class PqType>
Item<K, V> *DistributedPriorityQueue<K, V, Rlx, PqType>::find_min(V &value) {
  return dist_sl.get()->find_min(value);
}

template <class K, class V, int Rlx, class PqType>
int DistributedPriorityQueue<K, V, Rlx, PqType>::spy() {
  return dist_sl.get()->spy(this);
}

template <class K, class V, int Rlx, class PqType>
int DistributedPriorityQueue<K, V, Rlx, PqType>::size() {
  return dist_sl.get()->get_item_list_size();
}

template <class K, class V, int Rlx, class PqType>
int DistributedPriorityQueue<K, V, Rlx, PqType>::get_sl_id() const {
  return dist_sl.current_thread();
}

template <class K, class V, int Rlx, class PqType>
void DistributedPriorityQueue<K, V, Rlx, PqType>::set_sl_id() {
  dist_sl.get()->set_int_thread_id(get_sl_id());
}

template <class K, class V, int Rlx, class PqType>
void DistributedPriorityQueue<K, V, Rlx, PqType>::print() const {
  dist_sl.get()->print();
}

#endif // DISTRIBUTED_PRIORITY_QUEUE
