#ifndef LOCKFREE_VECTOR_H
#define LOCKFREE_VECTOR_H

#include <atomic>
#include <cassert>

template <class T> class LockFreeVector {
public:
  static constexpr int size = 128;

  LockFreeVector() {
    for (int i = 0; i < size; i++) {
      m_items[i] = nullptr;
    }
  }

  virtual ~LockFreeVector() {
    for (int i = 0; i < size; i++) {
      if (m_items[i] != nullptr) {
        T *item = m_items[i].load(std::memory_order_relaxed);
        delete item;
      }
    }
  }

  T *get(const int n) {
    T *item = m_items[n].load(std::memory_order_relaxed);
    if (item == nullptr) {
      item = new T;
      T *expected = nullptr;
      if (!m_items[n].compare_exchange_strong(expected, item)) {
        delete item;
        item = expected;
        assert(item != nullptr);
      }
    }
    return m_items[n];
  }

private:
  std::atomic<T *> m_items[size];
};

#endif // LOCKFREE_VECTOR_H
