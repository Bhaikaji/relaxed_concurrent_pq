/*
 *  Copyright 2014 Jakob Gruber
 *
 *  This file is part of kpqueue.
 *
 *  kpqueue is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  kpqueue is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with kpqueue.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __THREAD_LOCAL_PTR_H
#define __THREAD_LOCAL_PTR_H

#include "../../lib/logger.h"
#include "lockfree_vector.h"
#include <atomic>
#include <cassert>
#include <cstddef>
#include <vector>

namespace kpq {

void set_tid();
int32_t tid();
int32_t max_tid();
void reset_tid_values();

/**
 * A thread-local pointer to an element of type T, based on a dynamically
 * growing
 * array and the current thread id.
 */

template <class T> class thread_local_ptr {
public:
  thread_local_ptr() {
    // This is needed to pass the bench cases. This is because the value of
    // m_max_tid is not reset back to 0 at the start of each bench case, as it
    // is a static variable.
    reset_tid_values();
  }

  T *get() {
    set_tid();
    return get(tid());
  }

  T *get(const int32_t tid) {
    assert(tid < max_tid());
    return m_items.get(tid);
  }

  /** Returns the current thread id. */
  static size_t current_thread() { return tid(); }

  /** Returns the current thread count. */
  static size_t num_threads() { return max_tid(); }

private:
  LockFreeVector<T> m_items;
};

} // end kpq

#endif /* __THREAD_LOCAL_PTR_H */
