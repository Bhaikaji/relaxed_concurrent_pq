#include "thread_local_ptr.h"

namespace kpq {

static constexpr int TID_UNSET = -1;
static thread_local int m_tid = TID_UNSET;
static std::atomic<int> m_max_tid(0);

void set_tid() {
  if (m_tid == TID_UNSET) {
    m_tid = m_max_tid.fetch_add(1, std::memory_order_relaxed);
  }
}

int tid() { return m_tid; }

int max_tid() { return m_max_tid.load(std::memory_order_relaxed); }

void reset_tid_values() {
  m_tid = TID_UNSET;
  m_max_tid.store(0, std::memory_order_relaxed);
}

} // end kpq
