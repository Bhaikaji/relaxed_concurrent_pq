//
// Created by Ashok Adhikari on 4/5/17.
//

#include "../combined_nb_pq/combined_nb_pq.h"
#include "../dist_pq/dist_pq.h"
#include "../locked_pq/locked_pq.h"
#include "../shared_pq/shared_pq.h"

#include <gtest/gtest.h>
#include <iostream>

constexpr int RELAXATION = 64;

template <class K, class V, int Rlx> class Mock {
public:
  void insert(const K &key, const V &value) {}
};

#define mpq Mock<int, int, RELAXATION>
#define dpq DistributedPriorityQueue<int, int, RELAXATION, mpq>

using ::testing::TestWithParam;
using ::testing::Values;

typedef PQ<int, int, RELAXATION> *CreatePriorityQueueFunc();

PQ<int, int, RELAXATION> *CreateDistributedPQ() { return new dpq(); }

PQ<int, int, RELAXATION> *CreateSharedPQ() {
  return new SharedPQ<int, int, RELAXATION>();
};

PQ<int, int, RELAXATION> *CreateCombinedNBPQ() {
  return new CombinedNBPQ<int, int, RELAXATION>();
};

PQ<int, int, RELAXATION> *CreateLockedPQ() {
  return new LockedPQ<int, int, RELAXATION>();
};

class PriorityQueueTest : public TestWithParam<CreatePriorityQueueFunc *> {
public:
  virtual ~PriorityQueueTest() { delete pq_; }

  virtual void SetUp() { pq_ = (*GetParam())(); }

  virtual void TearDown() {
    delete pq_;
    pq_ = nullptr;
  }

protected:
  PQ<int, int, RELAXATION> *pq_;
};

TEST_P(PriorityQueueTest, SimpleInsertDeleteTest) {
  pq_->insert(2);
  int value;
  pq_->delete_min(value);
  ASSERT_EQ(2, value);
}

INSTANTIATE_TEST_CASE_P(PQ, PriorityQueueTest,
                        Values(&CreateDistributedPQ, &CreateSharedPQ,
                               &CreateLockedPQ, &CreateCombinedNBPQ););

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}