#include "../combined_nb_pq/combined_nb_pq.h"
#include "../combined_pq/combined_pq.h"
#include "../combined_pq/combined_pq.h"
#include "../shared_pq/shared_pq.h"

#include <algorithm>
#include <locked_pq.h>

int NUM_THREADS;
static unsigned long key = 0;
static const int benchmark_duration = 1;

static std::atomic<unsigned long> ops;
static std::atomic<bool> run_barrier(false);
static std::atomic<bool> stop(false);
static std::vector<bool> inserting(100);

enum WorkLoad { uniform, split, alternating };
static const char *enum_strings_workload[] = {"Uniform", "Split",
                                              "Alternating"};
const char *get_text_from_workload(int enum_val) {
  return enum_strings_workload[enum_val];
}

static const char *enum_strings_queue_type[] = {
    "dist_pq",           "shared_pq",
    "locked_pq",         "combined_locked_pq",
    "combined_nb_pq32",  "combined_nb_pq64",
    "combined_nb_pq128", "combined_nb_pq256",
    "combined_nb_pq4096"};
const char *get_text_from_queue_type(int enum_val) {
  return enum_strings_queue_type[enum_val];
}

enum QueueType {
  dist_pq,
  shared_pq,
  locked_pq,
  combined_locked_pq,
  combined_nb_pq32,
  combined_nb_pq64,
  combined_nb_pq128,
  combined_nb_pq256,
  combined_nb_pq4096,
  QueueType_ITEM_COUNT,
};

struct Result {
  int N;
  int Mops;
  WorkLoad work_load;
  QueueType q_type;

  Result(int N, int Mops, WorkLoad work_load, QueueType q_type)
      : N(N), Mops(Mops), work_load(work_load), q_type(q_type) {}

  void print_result() {
    LOG2("Num Threads: ", N, "Mops:", Mops, "Workload:",
         get_text_from_workload(work_load), "queue_type:",
         get_text_from_queue_type(q_type));
  }
};

void reset_variables() {
  ops.store(0, std::memory_order_relaxed);
  run_barrier.store(false, std::memory_order_relaxed);
  stop.store(false, std::memory_order_relaxed);
  std::fill(inserting.begin(), inserting.end(), true);
  key = 0;
}

void break_barrier(std::atomic_bool &barrier) {
  barrier.store(true, std::memory_order_relaxed);
}

void build_barrier(std::atomic_bool &barrier) {
  while (!barrier.load(std::memory_order_relaxed)) {
    // Wait.
  }
}

void timer(std::chrono::seconds sec) {
  LOG("Starting timer..");
  build_barrier(run_barrier);
  std::this_thread::sleep_for(sec);
  stop.store(true, std::memory_order_relaxed);
}

std::random_device rd; //seed
std::mt19937 gen(rd()); //ruleset for rd(merzenne twister)
std::uniform_int_distribution<>
    rng_32(std::llround(std::pow(2, 31)),
           std::llround(std::pow(2, 32))); // rng1 range

unsigned int get_rand_32_bit_int() {
  return rng_32(gen);
}

unsigned int get_key() { return get_rand_32_bit_int(); }

template <class Q> void run(Q &pq, int thread_id, WorkLoad work_load) {
  int value;

  build_barrier(run_barrier);

  while (!stop) {
    if (inserting[thread_id]) {
      pq.insert(get_key());
      ops.fetch_add(1, std::memory_order_relaxed);
      if (work_load == WorkLoad::alternating) {
        inserting[thread_id] = !inserting[thread_id];
      }
    } else {
      if (pq.delete_min(value)) {
        ops.fetch_add(1, std::memory_order_relaxed);
        if (work_load == WorkLoad::alternating) {
          inserting[thread_id] = !inserting[thread_id];
        }
      }
    }
  }
}

void workload_manager(WorkLoad work_load) {
  build_barrier(run_barrier);
  while (!stop) {
    switch (work_load) {
    case WorkLoad::uniform:
      std::this_thread::sleep_for(std::chrono::seconds(1));
      for (int i = 0; i < NUM_THREADS; ++i) {
        inserting[i] = !inserting[i];
      }
      break;
    case WorkLoad::split:
      break;
    case WorkLoad::alternating:
      break;
    default:
      break;
    }
  }
}

template <class Q> void run_benchmark(Q &pq, WorkLoad work_load) {
  std::thread threads[NUM_THREADS];

  for (int i = 0; i < NUM_THREADS; ++i) {
    threads[i] = std::thread(run<Q>, std::ref(pq), i, work_load);
    switch (work_load) {
    case WorkLoad::uniform:
      // Make it an inserting thread to start with.
      inserting[i] = true;
      break;
    case WorkLoad::split:
      // Half threads insert, while half of them delete.
      if (i % 2) {
        inserting[i] = true;
      } else {
        inserting[i] = false;
      }
      break;
    case WorkLoad::alternating:
      // Start everyone off with insertion.
      inserting[i] = true;
      break;
    default:
      break;
    }
  }

  std::thread wm = std::thread(workload_manager, work_load);
  std::thread tt = std::thread(timer, std::chrono::seconds(benchmark_duration));

  break_barrier(run_barrier);

  for (auto &th : threads) {
    th.join();
  }

  wm.join();
  tt.join();

  std::cout << "Mops = " << (ops / benchmark_duration) << std::endl;
}

template <class Q> void prefill(Q &pq) {
  for (int i = 0; i < pow(10, 6); ++i) {
    pq.insert(get_key());
  }
}

template <class Q> void do_benchnmark(WorkLoad work_load) {
  Q pq;
  reset_variables();
  prefill<Q>(std::ref(pq));
  run_benchmark<Q>(std::ref(pq), work_load);
}

void print_result_stdout(int u[][QueueType_ITEM_COUNT], int thread_count) {
  for (int i = 0; i < thread_count; ++i) {
    for (int j = 0; j < QueueType_ITEM_COUNT; ++j) {
      std::cout << u[i][j] << "\t";
    }
    std::cout << std::endl;
  }
}

int main(void) {

  std::vector<int> n_threads{1, 2, 4, 8, 16, 24, 32, 64, 72};

  std::vector<WorkLoad> work_loads{WorkLoad::uniform, WorkLoad::split,
                                   WorkLoad::alternating};

  std::vector<std::vector<Result>> data(work_loads.size());
  int index = 0;
  for (int i = 0; i < n_threads.size(); ++i) {
    const int n = n_threads[i];
    NUM_THREADS = n_threads[i];

    for (auto &work_load : work_loads) {
      // Distributed Queue
      do_benchnmark<
          DistributedPriorityQueue<int, int, 4096, SharedPQ<int, int, 4096>>>(
          work_load);
      data[work_load].push_back(
          Result(n, ops / benchmark_duration, work_load, QueueType::dist_pq));

      do_benchnmark<SharedPQ<int, int, 32>>(work_load);
      data[work_load].push_back(
          Result(n, ops / benchmark_duration, work_load, QueueType::shared_pq));

      do_benchnmark<LockedPQ<int, int, 32>>(work_load);
      data[work_load].push_back(
          Result(n, ops / benchmark_duration, work_load, QueueType::locked_pq));

      do_benchnmark<CombinedPQ<int, int, 32>>(work_load);
      data[work_load].push_back(Result(n, ops / benchmark_duration, work_load,
                                       QueueType::combined_locked_pq));

      do_benchnmark<CombinedNBPQ<int, int, 32>>(work_load);
      data[work_load].push_back(Result(n, ops / benchmark_duration, work_load,
                                       QueueType::combined_nb_pq32));

      do_benchnmark<CombinedNBPQ<int, int, 64>>(work_load);
      data[work_load].push_back(Result(n, ops / benchmark_duration, work_load,
                                       QueueType::combined_nb_pq64));

      do_benchnmark<LockedPQ<int, int, 128>>(work_load);
      data[work_load].push_back(Result(n, ops / benchmark_duration, work_load,
                                       QueueType::combined_nb_pq128));

      do_benchnmark<LockedPQ<int, int, 256>>(work_load);
      data[work_load].push_back(Result(n, ops / benchmark_duration, work_load,
                                       QueueType::combined_nb_pq256));

      do_benchnmark<LockedPQ<int, int, 4096>>(work_load);
      data[work_load].push_back(Result(n, ops / benchmark_duration, work_load,
                                       QueueType::combined_nb_pq4096));
    }
  }

  int u[n_threads.size()][QueueType_ITEM_COUNT];
  int s[n_threads.size()][QueueType_ITEM_COUNT];
  int a[n_threads.size()][QueueType_ITEM_COUNT];
  for (auto &d : data) {
    for (auto &v : d) {
      auto pos = std::find(n_threads.begin(), n_threads.end(), v.N) -
                 n_threads.begin();
      if (v.work_load == WorkLoad::uniform) {
        u[pos][v.q_type] = v.Mops;
      } else if (v.work_load == WorkLoad::split) {
        s[pos][v.q_type] = v.Mops;
      } else {
        a[pos][v.q_type] = v.Mops;
      }
      v.print_result();
    }
  }

  LOG2("Uniform");
  print_result_stdout(u, n_threads.size());
  LOG2("Split");
  print_result_stdout(s, n_threads.size());
  LOG2("Alternating");
  print_result_stdout(a, n_threads.size());
}