#ifndef LOCKED_SKIPLIST_H
#define LOCKED_SKIPLIST_H

#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "../lib/generic_skiplist.h"
#include "../lib/item.h"

template <class K, class V> class LockedSkipList : public SkipList<K, V> {
public:
  void insert(const K &key);
  void insert(const K &key, const V &value);
  void insert(Item<K, V> *item);

  // It deletes the element containing search_key, if it exists.
  void erase(const K &key);
  // Since erase as well as delete_min both use locks, erase can't be called
  // from delete_min. To avoid the deadlock, actual work of erase is
  // delegated to erase_impl. delete_min will now call erase_impl internally
  // which is not thread-safe.
  void erase_impl(const K &key);

  bool delete_min(V &value);
  Item<K, V> *find_min(V &value);

private:
  // Lock for the whole SkipList operations.
  std::mutex skip_lock;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V>
void LockedSkipList<K, V>::insert(Item<K, V> *item) {
  skip_lock.lock();
  std::vector<skv *> preds(this->template node_level(this->template head));
  this->template find(item->key, preds);

  // Create a new node.
  const int new_node_level = this->template random_level();
  auto new_node_ptr = this->template make_node(item, new_node_level);

  // Connect pointers of predecessors to new node and then to successors.
  for (int i = 0; i < new_node_level; ++i) {
    new_node_ptr->forward[i] = preds[i]->forward[i];
    preds[i]->forward[i] = new_node_ptr;
  }
  skip_lock.unlock();
}

template <class K, class V> void LockedSkipList<K, V>::insert(const K &key) {
  insert(key, key);
}

template <class K, class V>
void LockedSkipList<K, V>::insert(const K &key, const V &value) {
  Item<K, V> *item = new Item<K, V>(key, value);
  insert(item);
}

template <class K, class V>
void LockedSkipList<K, V>::erase_impl(const K &key) {
  std::vector<skv *> preds(this->template node_level(this->template head));
  if (!this->template find(key, preds)) {
    return;
  }
  auto node = preds[0]->forward[0];

  // Update pointers and delete node.
  for (int i = 0; i < this->template node_level(node); ++i) {
    preds[i]->forward[i] = node->forward[i];
  }
  // TODO(ashokadhikari) Memory leak. Can't delete item here because a
  // reference to this item may be held by other thread as a result of its
  // find_min operation. So, instead of a delete node->item, item should be
  // released to a item pool/handler implementation.
  delete node;
}

template <class K, class V> void LockedSkipList<K, V>::erase(const K &key) {
  skip_lock.lock();
  erase_impl(key);
  skip_lock.unlock();
}

template <class K, class V> bool LockedSkipList<K, V>::delete_min(V &value) {
  auto item = find_min(value);
  if (item == nullptr) {
    return false;
  }
  skip_lock.lock();
  if (item->taken) {
    skip_lock.unlock();
    return false;
  }
  item->taken = true;
  std::vector<skv *> preds(this->template node_level(this->template head));
  this->template find(item->key, preds);
  skip_lock.unlock();
  return true;
}

template <class K, class V>
Item<K, V> *LockedSkipList<K, V>::find_min(V &value) {
  skip_lock.lock();
  SkipNode<K, V> *current = this->template head->forward[0];
  while (current != this->template NIL) {
    if (current->item->taken) {
      current = current->forward[0];
    } else {
      value = current->item->value;
      skip_lock.unlock();
      return current->item;
    }
  }
  skip_lock.unlock();
  return nullptr;
}

#endif // LOCKED_SKIPLIST_H
