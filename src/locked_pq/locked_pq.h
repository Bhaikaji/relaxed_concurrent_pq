// A LockedPQ is priority queue based on SkipList. Concurrency is implemented
// using a coarse-grained locking.

#ifndef LOCKED_PRIORITY_QUEUE
#define LOCKED_PRIORITY_QUEUE

#include "../lib/item.h"
#include "../lib/pq.h"
#include "locked_skiplist.h"

template <class K, class V, int Rlx = 0> class LockedPQ : public PQ<K, V, Rlx> {
public:
  void insert(const K &key) override;

  void insert(const K &key, const V &value) override;

  void insert(Item<K, V> *item);

  bool delete_min(V &value) override;

  Item<K, V> *find_min(V &value);

private:
  LockedSkipList<K, V> locked_sl;
};

//
// Implementation details below. Clients should ignore it.
//

template <class K, class V, int Rlx>
void LockedPQ<K, V, Rlx>::insert(const K &key) {
  locked_sl.insert(key);
}

template <class K, class V, int Rlx>
void LockedPQ<K, V, Rlx>::insert(const K &key, const V &value) {
  locked_sl.insert(key, value);
}

template <class K, class V, int Rlx>
void LockedPQ<K, V, Rlx>::insert(Item<K, V> *item) {
  locked_sl.insert(item);
}

template <class K, class V, int Rlx>
bool LockedPQ<K, V, Rlx>::delete_min(V &value) {
  return locked_sl.delete_min(value);
}

template <class K, class V, int Rlx>
Item<K, V> *LockedPQ<K, V, Rlx>::find_min(V &value) {
  return locked_sl.find_min(value);
}

#endif // LOCKED_PRIORITY_QUEUE
